import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { RutasNoAutenticadas } from './Componentes/NoAutenticadas/RutasNoAutenticadas';
import { RutasAutenticadas } from './Componentes/Autenticadas/RutasAutenticadas';
import Store from './Store/Store';


console.disableYellowBox = ['Remote debugger']

export default class App extends React.Component {
  constructor() {
    super();
    this.state = { nombre: 'instagram-clone'};
  }

  render(){
  return (


 <Provider store={Store}>
    <NavigationContainer>
        <RutasNoAutenticadas/>
    </NavigationContainer>
  </Provider> 
  );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    
  },
});
