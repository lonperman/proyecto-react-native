import React, { Component } from 'react'
import { Text, StyleSheet, View, Button, TextInput } from 'react-native'
import { connect } from 'react-redux';
import SignUpForm from './Formas/SignUpForm';

class SignUp extends Component {
    registroDeUsuario = (values) => {
      console.log(values);
      this.props.registro(values);
    }
    render() {
        console.log(this.props.numero);
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <SignUpForm registro={this.registroDeUsuario}/>
                <Button 
                title='SignIn'
                onPress={ () => { navigation.goBack();}}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        backgroundColor: '#fff',
        paddingHorizontal: 16,
    },
});


//Inyeccion de propiedades desde redux
const mapStateToProps = (state) => ({
    numero: state.reducerPrueba,
});


const mapDispatchToProps = dispatch => ({
    registro: (values) => {
         dispatch({type: 'REGISTRO', datos: values});
     },
});


 //Conexion con redux
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);