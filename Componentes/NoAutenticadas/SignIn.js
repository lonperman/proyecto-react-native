import React, { Component } from 'react'
import { Text, StyleSheet, View, Button } from 'react-native'
import SignInForm from './Formas/SignInForm';

class SignIn extends Component {
    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <SignInForm />
                <Button 
                title='SignUp'
                onPress={() => { navigation.navigate('SignUp');}}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        backgroundColor: '#fff',
        paddingHorizontal: 16,
    },
});

export default SignIn;
