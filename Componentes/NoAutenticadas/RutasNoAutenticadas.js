import React from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import SignIn from './SignIn';
import SignUp from './SignUp';
const Stack = createStackNavigator();

const RutasNoAutenticadas = () => {
    return(
  
        <Stack.Navigator headerMode= 'none'>
            <Stack.Screen
            name= 'SignIn'
            component={SignIn}
            options={{
                title: 'Instagram-Clone',
             
            }}
            />
            <Stack.Screen
            name= 'SignUp'
            component={SignUp}
            options={{
                title: 'Instagram-Clone',
               
            }}
            />
        </Stack.Navigator>
   
    );
}

export { RutasNoAutenticadas };
