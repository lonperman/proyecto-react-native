import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Follow from './Follow';

const Tab = createMaterialTopTabNavigator();

const TabFollow = () => {
    return(
    <Tab.Navigator tabBarPosition= 'top'>
        <Tab.Screen name='Follow' component={Follow} />
        <Tab.Screen name='Followers' component={Follow} />
    </Tab.Navigator>
    );
}

export { TabFollow };