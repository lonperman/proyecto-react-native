import React from 'react'
import { Text, StyleSheet, View, Button} from 'react-native'

class Home extends React.Component {
    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <Text> Home </Text>
                <Button
                title= 'Autor'
                onPress={() => {navigation.navigate('Autor');}}
                />
                <Button 
                title='Comentarios'
                onPress={() => { navigation.navigate('Comentarios');}}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2C3E50',
    },
});

export default Home;