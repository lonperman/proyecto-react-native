import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

class Add extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text> Add </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2C3E50',
    },
})

export default Add;