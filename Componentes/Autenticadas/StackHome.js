import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './Home';
import Autor from './Profile';
import Publicacion from './Publicacion';
import Comentarios from './Comentarios';


const Stack = createStackNavigator();

const StackHome = () => {
     
    return(
        <Stack.Navigator>
          <Stack.Screen name='Home' component={Home} />
          <Stack.Screen name='Autor' component={Autor} />
          <Stack.Screen name='Publicacion' component={Publicacion} />
          <Stack.Screen 
          name='Comentarios' 
          component={Comentarios} 
           />
         </Stack.Navigator>
    );
}

export { StackHome };