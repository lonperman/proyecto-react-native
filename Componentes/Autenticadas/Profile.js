import React, { Component } from 'react'
import { Text, StyleSheet, View, Button } from 'react-native'

export default class Profile extends Component {
    render() {
        const { navigation } = this.props;
        return (

            <View style={styles.container}>
                <Text> Profile </Text>
                <Button
                title='Publicacion'
                onPress={() => { navigation.navigate('Publicacion');}}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2C3E50',
    },
})
