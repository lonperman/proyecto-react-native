import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { TabFollow } from './TabFollow';
import Autor from './Profile';
import Publicacion from './Publicacion';
import Comentarios from './Comentarios';

const Stack = createStackNavigator();

const StackFollow = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name='TabFollow' component={TabFollow} options={{headerShown: false}}/>
            <Stack.Screen name='Autor' component={Autor} />
            <Stack.Screen name='Publicacion' component={Publicacion} />
            <Stack.Screen name='Comentarios' component={Comentarios} />
        </Stack.Navigator>
    );
}

export { StackFollow };