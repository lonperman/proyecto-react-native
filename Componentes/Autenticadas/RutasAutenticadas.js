import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { StackHome }  from './StackHome';
import { StackSearch } from './StackSearch';
import Add from './Add';
import { StackFollow } from './StackFollow';
import Profile from './Profile';

const Tab = createBottomTabNavigator();

const RutasAutenticadas = () => {
    return(

        <Tab.Navigator tabBarOptions={safeAreaInset={top:'always'}}>
            <Tab.Screen name= 'Home' component={StackHome} />
            <Tab.Screen name= 'Search' component={StackSearch} />
            <Tab.Screen name= 'Add' component={Add} />
            <Tab.Screen name= 'Follow' component={StackFollow} />
            <Tab.Screen name= 'Profile' component={Profile}  />
        </Tab.Navigator>
    );
}

export { RutasAutenticadas };