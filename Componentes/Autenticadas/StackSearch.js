import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Search from './Search';
import Publicacion from './Publicacion';
import Autor from './Profile';
import Comentarios from './Comentarios';


const Stack = createStackNavigator();

const StackSearch = () => {

    return(
        <Stack.Navigator>
            <Stack.Screen name='Search' component={Search} />
            <Stack.Screen name='Publicacion' component={Publicacion} />
            <Stack.Screen name='Autor' component={Autor} />
            <Stack.Screen name='Comentarios' component={Comentarios} />
        </Stack.Navigator>
    )
}

export { StackSearch };